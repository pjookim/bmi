비만도 계산기 (BMI Calc)
=====================================
Copyright (C) 2015 pjookim <iam@pjookim.com>
----------------------------------------------------------------------------------------------
##스크린샷
![Screenshot.png](https://bitbucket.org/repo/77j78e/images/4114618629-Screenshot.png)


##정보
안녕하세요! 비만도 계산기의 오픈소스입니다.
많이 모자란 실력이지만 이 앱의 오픈소스가 안드로이드의 기초를 공부하는 분들께 도움이 됬으면 합니다.
궁금하신 점은 이메일 주소(iam@pjookim.com)으로 문의주시면 도와드리도록 노력하겠습니다.

* [PJDEV](http://dev.pjookim.com)
* iam@pjookim.com


##사용된 라이브러리
* [SnackBar](https://github.com/nispok/snackbar)


##LICENSE
* 별도의 언급이 있는 경우를 제외하고 비만도 계산기는 [아파치 라이선스 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)이 적용됩니다.
* 누구나 이 앱의 전체, 또는 일부를 사용할수 있으며, 수정 및 재배포 가능합니다.
* 수정 및 재배포시 앱 정보에 오픈소스 주소(https://bitbucket.org/pjookim/bmi)를 기재해주시기 바랍니다.
* 개발자에게 저작권이 있고 이 앱에 포함되어 있는 모든 소스의 상업적 이용을 허락하지 않습니다.
* 오픈소스 라이센스는 마켓에 올리는 앱의경우 마켓 설명 또는 앱안에서, apk로 올리는 앱의경우 파일을 첨부한 글에서 표시해야 합니다.